﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RandomItemGeneration
{
    class Item
    {
        public int rarity;
        public double rarityChance;
        public int Strength;
        public int Speed;
        public int Defense;
        public int Magic;
        public string Name;
        Random rand = new Random();


        public Item()
        {
            Name = "Random Item";
        }

        public void Randomize()
        {
            rarityChance = rand.Next(1, 100);

            if (rarityChance >= 96)
            {
                rarity = 5;
                Strength = rand.Next(80, 99);
                Speed = rand.Next(70, 99);
                Defense = rand.Next(80, 99);
                Magic = rand.Next(85, 99);
            }

            else if (rarityChance >= 88 && rarityChance < 96)
            {
                rarity = 4;
                Strength = rand.Next(75, 90);
                Speed = rand.Next(65, 80);
                Defense = rand.Next(60, 80);
                Magic = rand.Next(70, 85);
            }
            else if (rarityChance >= 70 && rarityChance < 88)
            {
                rarity = 3;
                Strength = rand.Next(55, 75);
                Speed = rand.Next(45, 65);
                Defense = rand.Next(40, 60);
                Magic = rand.Next(50, 70);
            }
            else if (rarityChance >= 40 && rarity < 70)
            {
                rarity = 2;
                Strength = rand.Next(30, 55);
                Speed = rand.Next(20, 45);
                Defense = rand.Next(15, 40);
                Magic = rand.Next(30, 50);
            }
            else
            {
                rarity = 1;
                Strength = rand.Next(10, 30);
                Speed = rand.Next(5, 20);
                Defense = rand.Next(3, 15);
                Magic = rand.Next(10, 30);

            }
        }


        public override string ToString()
        {
            String stats = "Name: " + Name + "\n Str: " + Strength + "\n Spd: " + Speed + "\n Def: " + Defense + "\n Magic: " + Magic + "\n Rarity: " + rarity;

            return stats;
        }
    }
}
